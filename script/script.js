//Теоретичні питання
//1. Опишіть своїми словами, що таке метод об'єкту
//Методом називається функція, яка є членом об'єкта. Властивість є значення або набір значень (у вигляді масиву або об'єкта), який є членом об'єкта і може містити будь який тип даних.

//2. Який тип даних може мати значення властивості об'єкта?
//Властивість об'єкта це значення будь якого типу яке належить об'єкту.

//3. Об'єкт це посилальний тип даних. Що означає це поняття?
//Посилальний тип – це “тип специфікації”. Ми не можемо явно використовувати його, але він використовується всередині мови.
//Значення посилального типу – це комбінація трьох значення (base, name, strict), де:
//base – це об’єкт.
//name – це назва властивості.
//strict – це true якщо діє use strict.

//Завдання

function createNewUser () {

    let firstName = prompt (`Введіть ваше ім'я`);
    let lastName = prompt (`Введіть ваше прізвище`);

    let newUser = {

        _firstName: firstName,
        _lastName: lastName,

        set firstName (value) {
            this._firstName = value;
        },
        get firstName () {
            return this._firstName
        },  

        set lastName (value) {
            this._lastName = value;
        },
        get lastName () {
            return this._lastName
        },  

        getLogin: function () {
            return (this.firstName[0].toLowerCase() + this.lastName.toLowerCase()); 
        }
    }
    return newUser 
}

let user = createNewUser ();

console.log(user);
console.log(user.getLogin());